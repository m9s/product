#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:product.category,childs:"
msgid "Children"
msgstr "Onderliggende niveaus"

#, fuzzy
msgctxt "field:product.category,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.category,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.category,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.category,name:"
msgid "Name"
msgstr "Naam"

msgctxt "field:product.category,parent:"
msgid "Parent"
msgstr "Bovenliggend niveau"

msgctxt "field:product.category,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.category,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.category,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.configuration,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.configuration,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

msgctxt "field:product.configuration,default_cost_price_method:"
msgid "Default Cost Method"
msgstr ""

#, fuzzy
msgctxt "field:product.configuration,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.configuration,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.configuration,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.configuration,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.configuration.default_cost_price_method,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.configuration.default_cost_price_method,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

msgctxt ""
"field:product.configuration.default_cost_price_method,default_cost_price_method:"
msgid "Default Cost Method"
msgstr ""

#, fuzzy
msgctxt "field:product.configuration.default_cost_price_method,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.configuration.default_cost_price_method,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.configuration.default_cost_price_method,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.configuration.default_cost_price_method,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

msgctxt "field:product.cost_price,company:"
msgid "Company"
msgstr ""

#, fuzzy
msgctxt "field:product.cost_price,cost_price:"
msgid "Cost Price"
msgstr "Kostprijs"

#, fuzzy
msgctxt "field:product.cost_price,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.cost_price,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.cost_price,id:"
msgid "ID"
msgstr "ID"

#, fuzzy
msgctxt "field:product.cost_price,product:"
msgid "Product"
msgstr "Productbeheer"

msgctxt "field:product.cost_price,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.cost_price,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.cost_price,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

msgctxt "field:product.cost_price_method,company:"
msgid "Company"
msgstr ""

msgctxt "field:product.cost_price_method,cost_price_method:"
msgid "Cost Price Method"
msgstr ""

#, fuzzy
msgctxt "field:product.cost_price_method,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.cost_price_method,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.cost_price_method,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.cost_price_method,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.cost_price_method,template:"
msgid "Template"
msgstr "Sjabloon"

#, fuzzy
msgctxt "field:product.cost_price_method,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.cost_price_method,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

msgctxt "field:product.list_price,company:"
msgid "Company"
msgstr ""

#, fuzzy
msgctxt "field:product.list_price,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.list_price,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.list_price,id:"
msgid "ID"
msgstr "ID"

#, fuzzy
msgctxt "field:product.list_price,list_price:"
msgid "List Price"
msgstr "Catalogusprijs"

msgctxt "field:product.list_price,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.list_price,template:"
msgid "Template"
msgstr "Sjabloon"

#, fuzzy
msgctxt "field:product.list_price,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.list_price,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.product,active:"
msgid "Active"
msgstr "Actief"

#, fuzzy
msgctxt "field:product.product,categories:"
msgid "Categories"
msgstr "Categorieën"

#, fuzzy
msgctxt "field:product.product,categories_all:"
msgid "Categories"
msgstr "Categorieën"

msgctxt "field:product.product,code:"
msgid "Code"
msgstr "Code"

msgctxt "field:product.product,consumable:"
msgid "Consumable"
msgstr ""

#, fuzzy
msgctxt "field:product.product,cost_price:"
msgid "Cost Price"
msgstr "Kostprijs"

msgctxt "field:product.product,cost_price_method:"
msgid "Cost Price Method"
msgstr ""

msgctxt "field:product.product,cost_price_methods:"
msgid "Cost Price Methods"
msgstr ""

#, fuzzy
msgctxt "field:product.product,cost_price_uom:"
msgid "Cost Price"
msgstr "Kostprijs"

#, fuzzy
msgctxt "field:product.product,cost_prices:"
msgid "Cost Prices"
msgstr "Kostprijs"

#, fuzzy
msgctxt "field:product.product,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.product,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.product,default_uom:"
msgid "Default UOM"
msgstr "Standaard ME"

msgctxt "field:product.product,default_uom_category:"
msgid "Default UOM Category"
msgstr ""

msgctxt "field:product.product,description:"
msgid "Description"
msgstr "Omschrijving"

#, fuzzy
msgctxt "field:product.product,id:"
msgid "ID"
msgstr "ID"

#, fuzzy
msgctxt "field:product.product,list_price:"
msgid "List Price"
msgstr "Catalogusprijs"

#, fuzzy
msgctxt "field:product.product,list_price_uom:"
msgid "List Price"
msgstr "Catalogusprijs"

#, fuzzy
msgctxt "field:product.product,list_prices:"
msgid "List Prices"
msgstr "Catalogusprijs"

#, fuzzy
msgctxt "field:product.product,name:"
msgid "Name"
msgstr "Naam"

msgctxt "field:product.product,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:product.product,template:"
msgid "Product Template"
msgstr "Productsjabloon"

#, fuzzy
msgctxt "field:product.product,type:"
msgid "Type"
msgstr "Type"

#, fuzzy
msgctxt "field:product.product,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.product,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

msgctxt "field:product.template,active:"
msgid "Active"
msgstr "Actief"

#, fuzzy
msgctxt "field:product.template,categories:"
msgid "Categories"
msgstr "Categorieën"

#, fuzzy
msgctxt "field:product.template,categories_all:"
msgid "Categories"
msgstr "Categorieën"

msgctxt "field:product.template,consumable:"
msgid "Consumable"
msgstr ""

msgctxt "field:product.template,cost_price:"
msgid "Cost Price"
msgstr "Kostprijs"

msgctxt "field:product.template,cost_price_method:"
msgid "Cost Price Method"
msgstr ""

msgctxt "field:product.template,cost_price_methods:"
msgid "Cost Price Methods"
msgstr ""

#, fuzzy
msgctxt "field:product.template,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.template,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

msgctxt "field:product.template,default_uom:"
msgid "Default UOM"
msgstr "Standaard ME"

msgctxt "field:product.template,default_uom_category:"
msgid "Default UOM Category"
msgstr ""

#, fuzzy
msgctxt "field:product.template,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.template,list_price:"
msgid "List Price"
msgstr "Catalogusprijs"

#, fuzzy
msgctxt "field:product.template,list_prices:"
msgid "List Prices"
msgstr "Catalogusprijs"

msgctxt "field:product.template,name:"
msgid "Name"
msgstr "Naam"

#, fuzzy
msgctxt "field:product.template,products:"
msgid "Variants"
msgstr "Producten"

msgctxt "field:product.template,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:product.template,type:"
msgid "Type"
msgstr "Type"

#, fuzzy
msgctxt "field:product.template,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.template,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.template-product.category,category:"
msgid "Category"
msgstr "Categorie"

#, fuzzy
msgctxt "field:product.template-product.category,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.template-product.category,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.template-product.category,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.template-product.category,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.template-product.category,template:"
msgid "Template"
msgstr "Sjabloon"

#, fuzzy
msgctxt "field:product.template-product.category,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.template-product.category,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.template-product.category.all,category:"
msgid "Category"
msgstr "Categorie"

#, fuzzy
msgctxt "field:product.template-product.category.all,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.template-product.category.all,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.template-product.category.all,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.template-product.category.all,rec_name:"
msgid "Record Name"
msgstr ""

#, fuzzy
msgctxt "field:product.template-product.category.all,template:"
msgid "Template"
msgstr "Sjabloon"

#, fuzzy
msgctxt "field:product.template-product.category.all,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.template-product.category.all,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

msgctxt "field:product.uom,active:"
msgid "Active"
msgstr "Actief"

#, fuzzy
msgctxt "field:product.uom,category:"
msgid "Category"
msgstr "ME categorie"

#, fuzzy
msgctxt "field:product.uom,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.uom,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

msgctxt "field:product.uom,digits:"
msgid "Display Digits"
msgstr "Zichtbare decimalen"

msgctxt "field:product.uom,factor:"
msgid "Factor"
msgstr "Faktor"

#, fuzzy
msgctxt "field:product.uom,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.uom,name:"
msgid "Name"
msgstr "Naam"

msgctxt "field:product.uom,rate:"
msgid "Rate"
msgstr "Verhouding"

msgctxt "field:product.uom,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:product.uom,rounding:"
msgid "Rounding Precision"
msgstr "Afronding"

msgctxt "field:product.uom,symbol:"
msgid "Symbol"
msgstr "Symbool"

#, fuzzy
msgctxt "field:product.uom,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.uom,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.uom.category,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:product.uom.category,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:product.uom.category,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:product.uom.category,name:"
msgid "Name"
msgstr "Naam"

msgctxt "field:product.uom.category,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:product.uom.category,uoms:"
msgid "Unit of Measures"
msgstr "Meeteenheid"

#, fuzzy
msgctxt "field:product.uom.category,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:product.uom.category,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

msgctxt "help:product.product,active:"
msgid "Uncheck to exclude from future use."
msgstr ""

msgctxt "help:product.template,active:"
msgid "Uncheck to exclude from future use."
msgstr ""

msgctxt "help:product.uom,active:"
msgid "Uncheck to exclude from future use."
msgstr ""

msgctxt "help:product.uom,factor:"
msgid ""
"The coefficient for the formula:\n"
"coef (base unit) = 1 (this unit)"
msgstr ""
"De coëfficiënt voor deze formule:\n"
"coëf (basiseenheid) = 1 (deze eenheid)"

msgctxt "help:product.uom,rate:"
msgid ""
"The coefficient for the formula:\n"
"1 (base unit) = coef (this unit)"
msgstr ""
"De coëfficiënt voor deze formule:\n"
"1(basiseenheid) = coëf (deze eenheid)"

#, fuzzy
msgctxt "model:ir.action,name:act_category_list"
msgid "Categories"
msgstr "Categorieën"

#, fuzzy
msgctxt "model:ir.action,name:act_category_tree"
msgid "Categories"
msgstr "Categories"

msgctxt "model:ir.action,name:act_product_configuration_form"
msgid "Product Configuration"
msgstr "Product Configuration"

#, fuzzy
msgctxt "model:ir.action,name:act_product_form"
msgid "Variants"
msgstr "Producten"

#, fuzzy
msgctxt "model:ir.action,name:act_product_from_template"
msgid "Variants"
msgstr "Producten"

#, fuzzy
msgctxt "model:ir.action,name:act_template_by_category"
msgid "Product by Category"
msgstr "Product categorie"

#, fuzzy
msgctxt "model:ir.action,name:act_template_form"
msgid "Products"
msgstr "Producten"

#, fuzzy
msgctxt "model:ir.action,name:act_uom_category_form"
msgid "Categories of Unit of Measure"
msgstr "Categories of Unit of Measure"

#, fuzzy
msgctxt "model:ir.action,name:act_uom_form"
msgid "Units of Measure"
msgstr "Units of Measure"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_category_list"
msgid "Categories"
msgstr "Categorieën"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_category_tree"
msgid "Categories"
msgstr "Categories"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_configuration"
msgid "Configuration"
msgstr "Instellingen"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_main_product"
msgid "Product"
msgstr "Product"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_product"
msgid "Variants"
msgstr "Producten"

msgctxt "model:ir.ui.menu,name:menu_product_configuration"
msgid "Product Configuration"
msgstr "Product Configuration"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_template"
msgid "Products"
msgstr "Producten"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_uom_category_form"
msgid "Categories"
msgstr "Categories"

#, fuzzy
msgctxt "model:ir.ui.menu,name:menu_uom_form"
msgid "Units of Measure"
msgstr "Units of Measure"

msgctxt "model:product.category,name:"
msgid "Product Category"
msgstr "Product categorie"

#, fuzzy
msgctxt "model:product.configuration,name:"
msgid "Product Configuration"
msgstr "Product Configuration"

msgctxt "model:product.configuration.default_cost_price_method,name:"
msgid "Product Configuration Default Cost Price Method"
msgstr ""

msgctxt "model:product.cost_price,name:"
msgid "Product Cost Price"
msgstr ""

msgctxt "model:product.cost_price_method,name:"
msgid "Product Cost Price Method"
msgstr ""

msgctxt "model:product.list_price,name:"
msgid "Product List Price"
msgstr ""

#, fuzzy
msgctxt "model:product.product,name:"
msgid "Product Variant"
msgstr "Product"

msgctxt "model:product.template,name:"
msgid "Product Template"
msgstr "Productsjabloon"

msgctxt "model:product.template-product.category,name:"
msgid "Template - Category"
msgstr ""

msgctxt "model:product.template-product.category.all,name:"
msgid "Template - Category All"
msgstr ""

msgctxt "model:product.uom,name:"
msgid "Unit of measure"
msgstr "Meeteenheid"

msgctxt "model:product.uom,name:uom_are"
msgid "Are"
msgstr "Are"

#, fuzzy
msgctxt "model:product.uom,name:uom_carat"
msgid "Carat"
msgstr "Carat"

msgctxt "model:product.uom,name:uom_centimeter"
msgid "centimeter"
msgstr "centimeter"

#, fuzzy
msgctxt "model:product.uom,name:uom_cubic_centimeter"
msgid "Cubic centimeter"
msgstr "Cubic centimeter"

msgctxt "model:product.uom,name:uom_cubic_foot"
msgid "Cubic foot"
msgstr "Cubic foot"

msgctxt "model:product.uom,name:uom_cubic_inch"
msgid "Cubic inch"
msgstr "Cubic inch"

#, fuzzy
msgctxt "model:product.uom,name:uom_cubic_meter"
msgid "Cubic meter"
msgstr "Cubic meter"

#, fuzzy
msgctxt "model:product.uom,name:uom_day"
msgid "Day"
msgstr "Day"

#, fuzzy
msgctxt "model:product.uom,name:uom_foot"
msgid "Foot"
msgstr "Foot"

msgctxt "model:product.uom,name:uom_gallon"
msgid "Gallon"
msgstr "Gallon"

msgctxt "model:product.uom,name:uom_gram"
msgid "Gram"
msgstr "Gram"

msgctxt "model:product.uom,name:uom_hectare"
msgid "Hectare"
msgstr "Hectare"

#, fuzzy
msgctxt "model:product.uom,name:uom_hour"
msgid "Hour"
msgstr "Hour"

msgctxt "model:product.uom,name:uom_inch"
msgid "Inch"
msgstr "Inch"

msgctxt "model:product.uom,name:uom_kilogram"
msgid "Kilogram"
msgstr "Kilogram"

msgctxt "model:product.uom,name:uom_kilometer"
msgid "Kilometer"
msgstr "Kilometer"

msgctxt "model:product.uom,name:uom_liter"
msgid "Liter"
msgstr "Liter"

msgctxt "model:product.uom,name:uom_meter"
msgid "Meter"
msgstr "Meter"

#, fuzzy
msgctxt "model:product.uom,name:uom_mile"
msgid "Mile"
msgstr "Mile"

#, fuzzy
msgctxt "model:product.uom,name:uom_millimeter"
msgid "Millimeter"
msgstr "Millimeter"

msgctxt "model:product.uom,name:uom_minute"
msgid "Minute"
msgstr "Minute"

#, fuzzy
msgctxt "model:product.uom,name:uom_ounce"
msgid "Ounce"
msgstr "Ounce"

#, fuzzy
msgctxt "model:product.uom,name:uom_pound"
msgid "Pound"
msgstr "Pound"

#, fuzzy
msgctxt "model:product.uom,name:uom_second"
msgid "Second"
msgstr "Second"

#, fuzzy
msgctxt "model:product.uom,name:uom_square_centimeter"
msgid "Square centimeter"
msgstr "Square centimeter"

msgctxt "model:product.uom,name:uom_square_foot"
msgid "Square foot"
msgstr "Square foot"

msgctxt "model:product.uom,name:uom_square_inch"
msgid "Square inch"
msgstr "Square inch"

#, fuzzy
msgctxt "model:product.uom,name:uom_square_meter"
msgid "Square meter"
msgstr "Square meter"

msgctxt "model:product.uom,name:uom_square_yard"
msgid "Square yard"
msgstr "Square yard"

#, fuzzy
msgctxt "model:product.uom,name:uom_unit"
msgid "Unit"
msgstr "Unit"

#, fuzzy
msgctxt "model:product.uom,name:uom_work_day"
msgid "Work Day"
msgstr "Work Day"

msgctxt "model:product.uom,name:uom_yard"
msgid "Yard"
msgstr "Yard"

msgctxt "model:product.uom,symbol:uom_are"
msgid "a"
msgstr "a"

msgctxt "model:product.uom,symbol:uom_carat"
msgid "c"
msgstr "c"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_centimeter"
msgid "cm"
msgstr "cm"

msgctxt "model:product.uom,symbol:uom_cubic_centimeter"
msgid "cm³"
msgstr "cm³"

msgctxt "model:product.uom,symbol:uom_cubic_foot"
msgid "ft³"
msgstr "ft³"

msgctxt "model:product.uom,symbol:uom_cubic_inch"
msgid "in³"
msgstr "in³"

msgctxt "model:product.uom,symbol:uom_cubic_meter"
msgid "m³"
msgstr "m³"

msgctxt "model:product.uom,symbol:uom_day"
msgid "d"
msgstr "d"

msgctxt "model:product.uom,symbol:uom_foot"
msgid "ft"
msgstr "ft"

msgctxt "model:product.uom,symbol:uom_gallon"
msgid "gal"
msgstr "gal"

msgctxt "model:product.uom,symbol:uom_gram"
msgid "g"
msgstr "g"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_hectare"
msgid "ha"
msgstr "ha"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_hour"
msgid "h"
msgstr "h"

msgctxt "model:product.uom,symbol:uom_inch"
msgid "in"
msgstr "in"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_kilogram"
msgid "kg"
msgstr "kg"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_kilometer"
msgid "km"
msgstr "km"

msgctxt "model:product.uom,symbol:uom_liter"
msgid "l"
msgstr "l"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_meter"
msgid "m"
msgstr "m"

msgctxt "model:product.uom,symbol:uom_mile"
msgid "mi"
msgstr "mi"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_millimeter"
msgid "mm"
msgstr "mm"

#, fuzzy
msgctxt "model:product.uom,symbol:uom_minute"
msgid "min"
msgstr "min"

msgctxt "model:product.uom,symbol:uom_ounce"
msgid "oz"
msgstr "oz"

msgctxt "model:product.uom,symbol:uom_pound"
msgid "lb"
msgstr "lb"

msgctxt "model:product.uom,symbol:uom_second"
msgid "s"
msgstr "s"

msgctxt "model:product.uom,symbol:uom_square_centimeter"
msgid "cm²"
msgstr "cm²"

msgctxt "model:product.uom,symbol:uom_square_foot"
msgid "ft²"
msgstr "ft²"

msgctxt "model:product.uom,symbol:uom_square_inch"
msgid "in²"
msgstr "in²"

msgctxt "model:product.uom,symbol:uom_square_meter"
msgid "m²"
msgstr "m²"

msgctxt "model:product.uom,symbol:uom_square_yard"
msgid "yd²"
msgstr "yd²"

msgctxt "model:product.uom,symbol:uom_unit"
msgid "u"
msgstr "u"

msgctxt "model:product.uom,symbol:uom_work_day"
msgid "wd"
msgstr "wd"

msgctxt "model:product.uom,symbol:uom_yard"
msgid "yd"
msgstr "yd"

msgctxt "model:product.uom.category,name:"
msgid "Product uom category"
msgstr "Product ME catergorie"

#, fuzzy
msgctxt "model:product.uom.category,name:uom_cat_length"
msgid "Length"
msgstr "Length"

#, fuzzy
msgctxt "model:product.uom.category,name:uom_cat_surface"
msgid "Surface"
msgstr "Surface"

#, fuzzy
msgctxt "model:product.uom.category,name:uom_cat_time"
msgid "Time"
msgstr "Time"

#, fuzzy
msgctxt "model:product.uom.category,name:uom_cat_unit"
msgid "Units"
msgstr "Units"

msgctxt "model:product.uom.category,name:uom_cat_volume"
msgid "Volume"
msgstr "Volume"

#, fuzzy
msgctxt "model:product.uom.category,name:uom_cat_weight"
msgid "Weight"
msgstr "Weight"

#, fuzzy
msgctxt "model:res.group,name:group_product_admin"
msgid "Product Administration"
msgstr "Product Administration"

#, fuzzy
msgctxt "selection:product.product,cost_price_method:"
msgid "Average"
msgstr "Gemiddeld"

#, fuzzy
msgctxt "selection:product.product,cost_price_method:"
msgid "Fixed"
msgstr "Vast"

#, fuzzy
msgctxt "selection:product.product,type:"
msgid "Assets"
msgstr "Activa"

msgctxt "selection:product.product,type:"
msgid "Goods"
msgstr ""

#, fuzzy
msgctxt "selection:product.product,type:"
msgid "Service"
msgstr "Service"

msgctxt "selection:product.template,cost_price_method:"
msgid "Average"
msgstr "Gemiddeld"

msgctxt "selection:product.template,cost_price_method:"
msgid "Fixed"
msgstr "Vast"

#, fuzzy
msgctxt "selection:product.template,type:"
msgid "Assets"
msgstr "Activa"

msgctxt "selection:product.template,type:"
msgid "Goods"
msgstr ""

msgctxt "selection:product.template,type:"
msgid "Service"
msgstr "Service"

msgctxt "view:product.category:"
msgid "Children"
msgstr "Onderliggende niveaus"

msgctxt "view:product.template:"
msgid "General"
msgstr "Algemeen"

#, fuzzy
msgctxt "view:product.configuration:"
msgid "Party Configuration"
msgstr "Relatie instellingen"

#, fuzzy
msgctxt "view:product.product:"
msgid "Product"
msgstr "Producten"

msgctxt "view:product.product:"
msgid "Products"
msgstr "Producten"

msgctxt "view:product.uom.category:"
msgid "Categories of Unit of Measure"
msgstr "Categorieën van meeteenheden"

msgctxt "view:product.uom.category:"
msgid "Category of Unit of Measure"
msgstr "Categorie van meeteenheden"

msgctxt "view:product.uom:"
msgid "Unit of Measure"
msgstr "Meeteenheid"

msgctxt "view:product.uom:"
msgid "Units of Measure"
msgstr "Meeteenheden"
